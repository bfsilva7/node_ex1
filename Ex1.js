const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

//clientes
app.get('/clientes',(req,res)=>{
    const nome = req.query.nome 
    res.send("{message:cliente " + nome + " encontrado}");
});

app.post('/clientes',(req,res)=>{
    const header = req.headers.access
    const nbody = req.body.nome
    const sbody = req.body.sobrenome
    let msg = ``

    if(header != "987654321"){
        msg = "falha"
    }else{
        msg = `sucesso: ${nbody} ${sbody}`
    } res.send(msg)
});

//funcionario
app.get('/funcionarios',(req,res)=>{
    const nome = req.query.nome //pegando o nome do funcionario
    res.send("{message:cliente " + nome + " encontrado}");
})

app.delete('/funcionarios/:nome',(req,res)=>{
    const header = req.headers.access
    const rnome = req.params.nome//pegando o valor de nome direto do link
    let msg = ``

    if(header != "987654321"){
        msg = "falha"
    }else{
        msg = `sucesso: ${rnome}`
    } res.send(msg)
})

app.put('/funcionarios',(req,res)=>{
    const header = req.headers.access
    const nbody = req.body.nome
    let msg = ``

    if(header != "987654321"){
        msg = "falha"
    }else{
        msg = `sucesso: ${nbody}`
    } res.send(msg)
})

app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});